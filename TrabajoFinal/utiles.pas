unit utiles;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, Controls, u_Estructuras;

function like(campo,campoRegistro:cadena30):boolean;
function confirmarOperacion(pregunta: cadena50; datos: cadena50): boolean;
function cortar(var s: string; inicio, tamano: integer): string;

implementation

function like(campo,campoRegistro:cadena30):boolean;
var
  coincidencia:boolean;
begin
  coincidencia := false;
  campo := UpperCase(campo);
  campoRegistro := UpperCase(campoRegistro);
  if (pos(campo, campoRegistro)<>0) then
     coincidencia:=true;
  like:=coincidencia;
end;

function confirmarOperacion(pregunta: cadena50; datos: cadena50): boolean;
var
  respuesta: boolean;
begin
  respuesta := false;
  if (MessageDlg(pregunta+#13#10+datos, mtConfirmation, [mbOK, mbCancel], 0)) = mrOK then
     respuesta := true;
  confirmarOperacion := respuesta;
end;

function cortar(var s: string; inicio, tamano: integer): string;
begin
  if inicio <= Length(s) then
     begin
       cortar := Copy(s, inicio, tamano);
       Delete(s, inicio, tamano);
     end
  else
    cortar := '';
end;

end.

