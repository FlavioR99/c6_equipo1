unit u_ABMCargos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, u_DAOCargos,u_Estructuras;

type

  { TfrmABMCargos }

  TfrmABMCargos = class(TForm)
    btnAceptar: TButton;
    btnSalir: TButton;
    cbxEstado: TCheckBox;
    edtTituloCargo: TEdit;
    edtMin: TEdit;
    edtID: TEdit;
    edtAbreviatura: TEdit;
    edtMax: TEdit;
    lblEstado: TLabel;
    lblTituloCargo: TLabel;
    lblTitulo: TLabel;
    lblSalarioMin: TLabel;
    lblID: TLabel;
    lblAbreviatura: TLabel;
    lblSalarioMax: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private

  public
    procedure inicializarComponentesC();
    procedure leerRegistro(var Pc: TCargo);
    procedure limpiarCampos();
  end;

var
  frmABMCargos: TfrmABMCargos;

implementation

{$R *.lfm}

{ TfrmABMCargos }

procedure TfrmABMCargos.inicializarComponentesC();
begin
  edtID.Text := IntToStr(proxID(fc));
  edtAbreviatura.Text := '';
  edtTituloCargo.Text := '';
  edtMin.Text := '';
  edtMax.Text := '';
  cbxEstado.Checked:=True;
  cbxEstado.Enabled:=False;
end;

procedure TfrmABMCargos.leerRegistro(var Pc: TCargo);
begin
  if((edtID.Text <> '')and(edtAbreviatura.text <> '')and(edtTituloCargo.text <> '')and(edtMin.text <> '')and(edtMax.text <> ''))then
    begin
      pc.id:= StrToInt(edtID.Text);
      if(lblTitulo.Caption <> 'Modificar')then
      begin
         if(buscarAbreviatura(fc, edtAbreviatura.Text))then
           ShowMessage('Debe colocar otra Abreviatura')
         else
           pc.abreviatura:= edtAbreviatura.Text;
      end
      else
      begin
         pc.abreviatura:= edtAbreviatura.Text;
      end;
      pc.tituloCargo:= edtTituloCargo.Text;
      pc.salarioMin := StrToFloat(edtMin.Text);
      if(StrToFloat(edtMax.Text)<=StrToFloat(edtMin.Text))then
        ShowMessage('El salario Máximo debe ser mayor al Mínimo')
      else
      pc.salarioMax := StrToFloat(edtMax.Text);
      if cbxEstado.Checked = true then
         pc.estado := 'Habilitado'
      else
         pc.estado := 'Inhabilitado';
    end
  else
  begin
    ShowMessage('Debe ingresar todos los campos');
  end;
end;
procedure TfrmABMCargos.limpiarCampos();
begin
  edtID.Text := '';
  edtAbreviatura.Text := '';
  edtTituloCargo.Text := '';
  edtMin.Text := '';
  edtMax.Text := '';
end;

procedure TfrmABMCargos.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmABMCargos.btnAceptarClick(Sender: TObject);
var
  Pc : TCargo;
  posicion : longInt;
begin
   try
      leerRegistro(Pc);
      posicion := buscarCargo(fc,Pc.id);

      if btnAceptar.Caption='Nuevo' then
         if (posicion = NO_ENCONTRADO) then
           begin
             altaCargo(fc,Pc);
             limpiarCampos;
             ShowMessage('Cargo Añadido');
           end
         else
           ShowMessage('El ID ya existe…')
      else
        if btnAceptar.Caption='Guardar' then
           begin
             modificarCargo(fc,pc,posicion);
             close;
           end
        else
            ShowMessage('Operación inválida...');
  except
    on e: EConvertError do
       showMessage(e.Message);
    on e: Exception do
       showMessage('Error inesperado: ' +  E.ClassName + #13#10 + e.Message);
  end;
end;

end.

