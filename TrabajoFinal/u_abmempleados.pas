unit u_ABMEmpleados;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  DateTimePicker, u_Estructuras, u_DAOEmpleados, u_DAOHistoricoCargos;

type

  { TfrmABM }

  TfrmABM = class(TForm)
    btnSalir: TButton;
    btnAceptar: TButton;
    chkEstado: TCheckBox;
    cmbManager: TComboBox;
    cmbCargo: TComboBox;
    cmbDepto: TComboBox;
    DateTimePicker1: TDateTimePicker;
    dtpHoy: TDateTimePicker;
    edtID: TEdit;
    edtNombre: TEdit;
    edtApellido: TEdit;
    edtEmail: TEdit;
    edtTelefono: TEdit;
    edtSalario: TEdit;
    edtPorcComision: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    lblFecha: TLabel;
    lblCargoInicial: TLabel;
    lblEstado: TLabel;
    lblManager: TLabel;
    lblEmpleados: TLabel;
    lblPorcentajeCom: TLabel;
    lblID: TLabel;
    lblNombre: TLabel;
    lblApellido: TLabel;
    lblEmail: TLabel;
    lblDepartamento: TLabel;
    lblTelefono: TLabel;
    lblFechaIngreso: TLabel;
    lblCargo: TLabel;
    lblSalario: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private

  public
  procedure leerRegistro(var emp : TEmpleado);
  function buscarCargos(var vC: TVectorCargo ; var n: Integer): LongInt;
  function buscarDepto(var vD: TVectorDepartamento ; var n: Integer): LongInt;
  procedure limpiarCampos();
  function existeMail(var vE: TVectorEmpleado ; var n: Integer): Boolean;
  function validarRegistro(var emp: TEmpleado): boolean;
  function posicionEmpleado(var vE: TVectorEmpleado ; var n: Integer): LongInt;
  end;

var
  frmABM: TfrmABM;
  aux2:THistoricoCargo;

implementation

{$R *.lfm}

{ TfrmABM }

function TfrmABM.posicionEmpleado(var vE: TVectorEmpleado ; var n: Integer): LongInt;
var
  posicion, i: Integer;
  apellido: String;
begin
  posicion:= 0;
  apellido:= cmbManager.Text;
  i:= 1;
  while (i <= n) and (posicion = 0) do
  begin
    if apellido = vE[i].apellido then
       posicion:= i;
    i:= i + 1;
  end;
  posicionEmpleado:= posicion;
end;

function TfrmABM.existeMail(var vE: TVectorEmpleado ; var n: Integer): Boolean;
var
  flag : boolean;
  i : Integer;
  mail : String;
begin
  i := 1;
  flag := False;
  mail := edtEmail.Text;
  while (i <= n) and (flag = false) do
  begin
    if mail = vE[i].email then
       flag := true
    else
        i := i + 1;
  end;
  existeMail := flag;
end;

function TfrmABM.buscarCargos(var vC: TVectorCargo ; var n: Integer): LongInt;
var
  i , posicion: Integer;
  nombre: String;
begin
  i:= 1;
  posicion:= 0;
  nombre:= cmbCargo.Text;
  while (i <= n) and (posicion = 0) do
  begin
    if nombre = vC[i].tituloCargo then
       posicion:= i;
    i:= i + 1;
  end;
  buscarCargos:= posicion;
end;

function TfrmABM.buscarDepto(var vD: TVectorDepartamento ; var n: Integer): LongInt;
var
  i , posicion : Integer;
  nombre: String;
begin
  i:= 1;
  posicion:= 0;
  nombre:= cmbDepto.Text;
  while (i <= n) and (posicion = 0) do
  begin
    if nombre = vD[i].nombre then
       posicion:= i;
    i:= i + 1;
  end;
  buscarDepto:= posicion;
end;

procedure TfrmABM.limpiarCampos();
var
  i: Integer;
begin
  edtNombre.Text := '';
  edtApellido.Text := '';
  edtEmail.Text := '';
  edtTelefono.Text := '';
  edtSalario.Text := '';
  edtPorcComision.Text := '';
  cmbDepto.Text := '';
  cmbManager.Text := '';
  cmbCargo.Text := '';
  with cmbCargo do
  begin
    Items.BeginUpdate;
    Items.Clear;
    for i:= 1 to nCargo do
        Items.Add(vCargo[i].tituloCargo);
    Items.EndUpdate;
  end;
  with cmbDepto do
  begin
    Items.BeginUpdate;
    Items.Clear;
    for i:= 1 to nDepartamento do
        Items.Add(vDepartamento[i].nombre);
    Items.EndUpdate;
  end;
  with cmbManager do
  begin
    Items.BeginUpdate;
    Items.Clear;
    for i:= 1 to nEmpleado do
        Items.Add(vEmpleado[i].apellido);
    Items.EndUpdate;
  end;
end;

procedure TfrmABM.leerRegistro(var emp : TEmpleado);
begin
  try
     emp.id := StrToInt(edtID.Text);
     emp.nombre := edtNombre.Text;
     emp.apellido := edtApellido.Text;
     emp.email := edtEmail.Text;
     emp.telefono := edtTelefono.Text;
     emp.cargo := vCargo[buscarCargos(vCargo , nCargo)];
     emp.salario := StrToFloat(edtSalario.Text);
     emp.porcentajeDeComision := StrToFloat(edtPorcComision.Text);
     emp.departamento := vDepartamento[buscarDepto(vDepartamento , nDepartamento)];
     emp.manager := vEmpleado[posicionEmpleado(vEmpleado , nEmpleado)].apellido;
     if chkEstado.Checked then
         emp.estado := 'Habilitado'
      else
          emp.estado := 'Inhabilitado';
      emp.fechaDeIngreso := DateTimePicker1.Date;
      except
      on E : Exception do
      ShowMessage('Error: los datos ingresados no son válidos');
      on E: EConvertError do
      ShowMessage('Datos no Compatibles');
      end;
  end;

function TfrmABM.validarRegistro(var emp: TEmpleado): boolean;
var
  flag: boolean;
begin
  if (emp.nombre <> '') and (emp.apellido <> '') and (emp.email <> '') and
  (emp.cargo.tituloCargo <> '') and (emp.salario > 0) and ((emp.porcentajeDeComision >= 0) and
  (emp.porcentajeDeComision <= 1)) and (emp.departamento.nombre <> '') then
       flag:= true
   else
       flag:= false;
  validarRegistro:= flag;
end;

procedure cargarHistCargos(var hist: THistoricoCargo ; var pe: TEmpleado);
begin
  hist.id := pe.id;
  hist.empleado := pe;
  hist.cargo := pe.cargo;
  hist.departamento := pe.departamento;
  hist.fechaInicio := pe.fechaDeIngreso;
  frmABM.Label1.Caption := DateToStr(pe.fechaDeIngreso);
  altaHistCargos(fhc , hist);
end;

procedure TfrmABM.btnAceptarClick(Sender: TObject);
var
  vHC: TVectorHistoricoCargos;
  posicion, pos : LongInt;
  aux: THistoricoCargo;
begin
  leerRegistro(pe);
  if validarRegistro(pe) then
     begin
        posicion := buscarEmpleado(fe , StrToInt(edtID.Text));
        if btnAceptar.Caption = 'Alta' then
        begin
          altaEmpleado(fe , pe);
          phc.fechaFin := '-';
          cargarHistCargos(phc , pe);
          close;
        end
        else
        if (btnAceptar.Caption = 'Modificar') and (posicion <> NO_ENCONTRADO) then
        begin
          modificarEmpleado(fe , pe , posicion);
          Label1.Caption := DateToStr(pe.fechaDeIngreso);
          Label2.Caption := 'OK';
          if lblCargoInicial.Caption <> pe.cargo.tituloCargo then
             begin
               pos := buscarHistCargos(fhc,pe.id);
               aux.id:=pe.id;
               aux.empleado.apellido:=pe.apellido;
               aux.fechaInicio:=StrToDate(lblFecha.caption);
               aux.fechaFin:=DateToStr(frmABM.DateTimePicker1.Date);
               aux.cargo:=aux2.cargo;
               aux.departamento:=aux2.departamento;
               modificarHistCargos(fhc,aux,pos);
               cargarHistCargos(phc , pe);
             end;
          limpiarCampos();
          close;
        end
        else
        ShowMessage('ID no encontrado');
     end
  else
      ShowMessage('Datos incorrectos');
end;

procedure TfrmABM.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmABM.FormActivate(Sender: TObject);
begin
   dtpHoy.Date := Now;
   DateTimePicker1.Date:= Now;
   leerRegistro(pe);
   aux2.cargo:=pe.cargo;
   aux2.departamento:=pe.departamento;
end;


end.

