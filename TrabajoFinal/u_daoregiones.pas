unit u_DAORegiones;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils , u_Estructuras, Dialogs;
procedure crearFichero(var f:TFicheroRegiones);
//procedure mostrarRegiones(var f:TFicheroRegiones);
procedure cargarFicheroRegion (var f: TFicheroRegiones);
procedure cargarVector(var v:TVectorRegion;var n:integer);
implementation
procedure crearFichero(var f:TFicheroRegiones);
begin
  assignFile(f,FICHERO_REGIONES);
  try
    reset(f);
  except
    rewrite(f);
    closefile(f);
  end;
end;
procedure cargarFicheroRegion (var f : TFicheroRegiones);
var
  i : integer;
  rRegion : TRegion;
begin
  crearFichero(f);
  reset(f);
  rRegion.id := 1;
  rRegion.nombre := 'Europa';
  write(f,rRegion);
  rRegion.id := 2;
  rRegion.nombre := 'América';
  write(f,rRegion);
  rRegion.id := 3;
  rRegion.nombre := 'Asia';
  write(f,rRegion);
  rRegion.id := 4;
  rRegion.nombre := 'Este Medio y África';
  write(f,rRegion);
  closeFile(f);
end;
procedure cargarVector(var v:TVectorRegion;var n:integer);
var
  fila : integer;
  rRegion : TRegion;
begin
  cargarFicheroRegion(f);
   try
    reset(f);
    fila := 0;
    N := 0;
    while not eof(f) do
       begin
        read(f,rRegion);
          begin
            fila := fila + 1;
            vRegion[fila].id := rRegion.id;
            vRegion[fila].nombre := rRegion.nombre;
          end;
       end;
    n := fila;
    closeFile(f);
    except
      on E: EInOutError do
      ShowMessage('Error: '+E.ClassName+'/'+E.Message);
  end;
end;
{procedure mostrarRegiones (var f : TFicheroRegiones);
var
 fila : Integer;
 R : TRegion;
begin
  try
    reset(f);
    frmRegiones.grdRegiones.RowCount := 1;
    while not eof(f) do
    begin
      read(f , R);
      frmRegiones.grdRegiones.RowCount := frmRegiones.grdRegiones.RowCount + 1;
      fila :=  frmRegiones.grdRegiones.RowCount - 1;
      frmRegiones.grdRegiones.Cells[1 , fila] := IntToStr(R.id);
      frmRegiones.grdRegiones.Cells[2 , fila] := R.nombre;
    end;
  finally
  end;
end;}

end.

