unit u_HistoricoCargos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids ,
  u_DAOHistoricoCargos, u_Estructuras, u_ABMEmpleados;

type

  { TfrmHistoricoCargo }

  TfrmHistoricoCargo = class(TForm)
    btnSalir: TButton;
    lblHistoricoCargo: TLabel;
    grdHistCargo: TStringGrid;
    procedure btnSalirClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
    procedure mostrarVectorHistoricoCargos(vHC: TVectorHistoricoCargos ; nHC: Integer);
    procedure modificarVectorHistoricoCargos();
  end;

var
  frmHistoricoCargo: TfrmHistoricoCargo;

implementation

{$R *.lfm}

{ TfrmHistoricoCargo }

procedure TfrmHistoricoCargo.mostrarVectorHistoricoCargos(vHC: TVectorHistoricoCargos ; nHC: Integer);
var
  i, fila: Integer;
begin
  grdHistCargo.RowCount := 1;
  for i:= 1 to nHC do
  begin
    grdHistCargo.RowCount:= grdHistCargo.RowCount + 1;
    fila:= grdHistCargo.RowCount - 1;
    grdHistCargo.Cells[1 , fila]:= IntToStr(vHC[i].id);
    grdHistCargo.Cells[2 , fila]:= vHC[i].empleado.apellido;
    grdHistCargo.Cells[3 , fila]:= DateTimeToStr(vHC[i].fechaInicio);
    grdHistCargo.Cells[4 , fila]:= (vHC[i].fechaFin);
    grdHistCargo.Cells[5 , fila]:= vHC[i].cargo.tituloCargo;
    grdHistCargo.Cells[6 , fila]:= vHC[i].departamento.nombre;
  end;
end;

procedure TfrmHistoricoCargo.modificarVectorHistoricoCargos();
var
  i: LongInt;
begin
  i := buscarHistCargos(fhc, StrToInt(frmABM.edtID.Text));
  grdHistCargo.Cells[4, (i+1)]:= frmABM.Label1.Caption;
end;

procedure TfrmHistoricoCargo.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmHistoricoCargo.FormActivate(Sender: TObject);
begin
  cargarVectorHistCargos(vHistoricoCargo , nHistoricoCargo);
  mostrarVectorHistoricoCargos(vHistoricoCargo , nHistoricoCargo);
  {if frmABM.Label2.Caption = 'OK' then
    begin
    modificarVectorHistoricoCargos();
    frmABM.Label2.Caption := '-';
    end;}
end;

procedure TfrmHistoricoCargo.FormCreate(Sender: TObject);
begin
  crearFicheroHistCargos(fhc);
  cargarVectorHistCargos(vHistoricoCargo , nHistoricoCargo);
end;

end.

