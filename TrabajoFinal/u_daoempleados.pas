unit u_DAOEmpleados;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, u_Estructuras, Dialogs;
procedure altaEmpleado(var fE: TFicheroEmpleado ; empleado : TEmpleado);
procedure crearFicheroEmpleados(var fE: TFicheroEmpleado);
procedure cargarFicheroEmpleados(var fE:TFicheroEmpleado);
procedure cargarVectorEmpleadosProm(var vE : TVectorEmpleado ; var n : Integer ; buscado : string);
procedure cargarVectorEmpleadosManager(var vE : TVectorEmpleado ; var n : Integer ; buscado2 : string);
procedure cargarVectorEmpleados(var vE : TVectorEmpleado ; var n : Integer);
function buscarEmpleado(var fE: TFicheroEmpleado ; idE: Integer): LongInt;
function SumarSalarios(var fE: TFicheroEmpleado ; deptBuscado : String): Real;
procedure modificarEmpleado(var fE: TFicheroEmpleado ; empleado : TEmpleado ; posicion : LongInt);
function getSiguienteIdEmpleado():integer;
function paises(vEmpleado: TVectorEmpleado; pais: integer):integer;

var
  cont,cont2: Integer;

implementation

function paises(vEmpleado: TVectorEmpleado; pais: integer):integer;
var
  europa, america, asia, africa, i, p, id: integer;
begin
 europa := 0;
 america := 0;
 asia := 0;
 africa := 0;
 for i:=1 to nEmpleado do
 begin
 id := vEmpleado[i].departamento.pais.id;
 case id of
 1: begin
    europa := europa + 1;
   end;
 2: begin
    asia := asia + 1;
   end;
 3: begin
    america := america + 1;
   end;
 4: begin
    america := america + 1;
   end;
 5: begin
    africa := africa + 1;
   end;
 6: begin
    america := america + 1;
   end;
 end;
 end;
 if pais = 1 then
  p:= europa
 else
   if pais = 2 then
    p:= america
   else
     if pais = 3 then
      p:= asia
     else
       p := africa;

  paises := p;
end;

function getSiguienteIdEmpleado():integer;
var
 id: integer;
 pe: TEmpleado;
begin
 id := 0;
 reset(fe);
 while not eof(fe) do
    begin
      read(fe, pe);
      inc(id);
    end;
 CloseFile(fe);
 getSiguienteIdEmpleado := id + 1;
end;

function buscarEmpleado(var fE: TFicheroEmpleado ; idE: Integer): LongInt;
var
  i , posicion : LongInt;
  empleado : TEmpleado;
begin
  try
    Reset(fE);
    posicion := NO_ENCONTRADO;
    i := 0;
    while (not EOF(fE)) and (posicion = NO_ENCONTRADO) do
    begin
      Read(fE , empleado);
      if idE = empleado.id then
         posicion := i
      else
        i := i + 1;
    end;
    CloseFile(fE);
  finally
    buscarEmpleado := posicion;
  end;
end;

function SumarSalarios(var fE: TFicheroEmpleado ; deptBuscado : String): Real;
var
  i : Real;
  empleado : TEmpleado;
begin
  try
    Reset(fE);
    i := 0;
    while (not EOF(fE)) do
    begin
      Read(fE , empleado);
      if empleado.departamento.nombre = deptBuscado then
         i := i + empleado.salario;
    end;
    CloseFile(fE);
  finally
    SumarSalarios := i;
  end;
end;

procedure modificarEmpleado(var fE: TFicheroEmpleado ; empleado : TEmpleado ; posicion : LongInt);
begin
  try
    Reset(fE);
    Seek(fE , posicion);
    Write(fE , empleado);
  finally
    CloseFile(fE);
  end;
end;

procedure altaEmpleado(var fE: TFicheroEmpleado; empleado: TEmpleado);
begin
  try
    reset(fE);
    seek(fE , FileSize(fE));
    Write(fE , empleado);
  finally
    CloseFile(fE);
  end;
end;
procedure crearFicheroEmpleados(var fE: TFicheroEmpleado);
begin
  AssignFile(fE , FICHERO_EMPLEADOS);
  try
    Reset(fE);
  except
    Rewrite(fE);
    CloseFile(fE);
  end;
end;

procedure cargarFicheroEmpleados(var fE:TFicheroEmpleado);
var
  pe :TEmpleado;
begin
  crearFicheroEmpleados(fE);
  reset(fE);

  pe.id:= 1;
  pe.nombre:='Michael';
  pe.apellido:='Morrison';
  pe.email:='MorrisonM@gmail.com';
  pe.telefono:='3884204956';
  pe.fechaDeIngreso:= StrToDateTime('20/08/1999');
  pe.cargo.tituloCargo:='Presidente';
  pe.salario:=37000;
  pe.porcentajeDeComision:=1;
  pe.manager:='-';
  pe.departamento.nombre:='Administración';
  pe.estado:='Habilitado';
  write(fE,pe);

  pe.id:= 2;
  pe.nombre:='William';
  pe.apellido:='Jobs';
  pe.email:='WillJob@gmail.com';
  pe.telefono:='3885103245';
  pe.fechaDeIngreso:= StrToDateTime('24/06/2003');
  pe.cargo.tituloCargo:='VicePresidente';
  pe.salario:=29000;
  pe.porcentajeDeComision:=1;
  pe.manager:='Morrison';
  pe.departamento.nombre:='Administración';
  pe.estado:='Habilitado';
  write(fE,pe);

  pe.id:= 3;
  pe.nombre:='April';
  pe.apellido:='Sanchez';
  pe.email:='AprilSanchez@gmail.com';
  pe.telefono:='3885022955';
  pe.fechaDeIngreso:= StrToDateTime('04/03/2004');
  pe.cargo.tituloCargo:='Administrador de Marketing';
  pe.salario:=12000;
  pe.porcentajeDeComision:=1;
  pe.manager:='Morrison';
  pe.departamento.nombre:='Marketing';
  pe.estado:='Habilitado';
  write(fE,pe);

  closeFile(fE);
end;

procedure cargarVectorEmpleadosProm(var vE : TVectorEmpleado ; var n : Integer ; buscado : string);
var
  fila : Integer;
  empleado : TEmpleado;
begin
  try
    Reset(fe);
    fila := 0;
    cont:=0;
    n := 0;
    while not EOF(fe) do
    begin
      Read(fe , empleado);
      begin
        if(empleado.departamento.nombre = buscado)then
           begin
              fila := fila + 1;
              vE[fila].nombre := empleado.nombre;
              vE[fila].apellido := empleado.apellido;
              vE[fila].email := empleado.email;
              vE[fila].telefono := empleado.telefono;
              vE[fila].estado := empleado.estado;
              vE[fila].departamento := empleado.departamento;
              vE[fila].cargo := empleado.cargo;
              vE[fila].fechaDeIngreso := empleado.fechaDeIngreso;
              vE[fila].id := empleado.id;
              vE[fila].manager := empleado.manager;
              vE[fila].porcentajeDeComision := empleado.porcentajeDeComision;
              vE[fila].salario := empleado.salario;
              cont:= cont + 1;
           end;
      end;
    end;
    n := fila;
    CloseFile(fe);
  except
    on E : EInOutError do
    //Acciones si ocurre un error
  end;
end;

procedure cargarVectorEmpleadosManager(var vE : TVectorEmpleado ; var n : Integer ; buscado2 : string);
var
  fila : Integer;
  empleado : TEmpleado;
begin
  try
    Reset(fe);
    fila := 0;
    cont2:=0;
    n := 0;
    while not EOF(fe) do
    begin
      Read(fe , empleado);
      begin
        if(empleado.manager = buscado2)then
           begin
              fila := fila + 1;
              vE[fila].nombre := empleado.nombre;
              vE[fila].apellido := empleado.apellido;
              vE[fila].email := empleado.email;
              vE[fila].telefono := empleado.telefono;
              vE[fila].estado := empleado.estado;
              vE[fila].departamento := empleado.departamento;
              vE[fila].cargo := empleado.cargo;
              vE[fila].fechaDeIngreso := empleado.fechaDeIngreso;
              vE[fila].id := empleado.id;
              vE[fila].manager := empleado.manager;
              vE[fila].porcentajeDeComision := empleado.porcentajeDeComision;
              vE[fila].salario := empleado.salario;
              cont2:= cont2 + 1;
           end;
      end;
    end;
    n := fila;
    CloseFile(fe);
  except
    on E : EInOutError do
    //Acciones si ocurre un error
  end;
end;

procedure cargarVectorEmpleados(var vE : TVectorEmpleado ; var n : Integer);
var
  fila : Integer;
  empleado : TEmpleado;
begin
  try
    Reset(fe);
    fila := 0;
    n := 0;
    while not EOF(fe) do
    begin
      Read(fe , empleado);
      begin
        fila := fila + 1;
        vE[fila].nombre := empleado.nombre;
        vE[fila].apellido := empleado.apellido;
        vE[fila].email := empleado.email;
        vE[fila].telefono := empleado.telefono;
        vE[fila].estado := empleado.estado;
        vE[fila].departamento := empleado.departamento;
        vE[fila].cargo := empleado.cargo;
        vE[fila].fechaDeIngreso := empleado.fechaDeIngreso;
        vE[fila].id := empleado.id;
        vE[fila].manager := empleado.manager;
        vE[fila].porcentajeDeComision := empleado.porcentajeDeComision;
        vE[fila].salario := empleado.salario;
      end;
    end;
    n := fila;
    CloseFile(fe);
  except
    on E : EInOutError do
    //Acciones si ocurre un error
  end;
end;


end.

