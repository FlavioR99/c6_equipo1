unit u_Departamentos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids,
  u_DAODepartamentos, u_Estructuras, u_ABMDepartamento, utiles;

type

  { TfrmDepartamentos }

  TfrmDepartamentos = class(TForm)
    btnAlta: TButton;
    btnModificar: TButton;
    btnSalir: TButton;
    btnHabilitar: TButton;
    btnInhabilitar: TButton;
    edtBuscar: TEdit;
    grdDepartamentos: TStringGrid;
    lblBuscar: TLabel;
    lblDepartamentos: TLabel;
    procedure btnAltaClick(Sender: TObject);
    procedure btnHabilitarClick(Sender: TObject);
    procedure btnInhabilitarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure edtBuscarChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
  procedure cargarGrillaDepartamento(v:TVectorDepartamento; n: integer);
  procedure mostrarRegistro();
  procedure limpiarCampos();
  end;

var
  frmDepartamentos: TfrmDepartamentos;

implementation

{$R *.lfm}

{ TfrmDepartamentos }

procedure TfrmDepartamentos.cargarGrillaDepartamento(v:TVectorDepartamento; n: integer);
var
  i,fila : integer;
begin
 grdDepartamentos.RowCount := 1;
 for i := 1 to n do
      begin
      grdDepartamentos.RowCount:= grdDepartamentos.RowCount + 1;
      fila :=  grdDepartamentos.RowCount - 1;
      grdDepartamentos.Cells[1,fila] := IntToStr(v[i].id);
      grdDepartamentos.Cells[2,fila] := v[i].nombre;
      grdDepartamentos.Cells[3,fila] := v[i].direccion;
      grdDepartamentos.Cells[4,fila] := v[i].codigoPostal;
      grdDepartamentos.Cells[5,fila] := IdPais(v[i].pais.id);
      grdDepartamentos.Cells[6,fila] := v[i].estado;
      end;
end;

procedure TfrmDepartamentos.mostrarRegistro();
var
  estado: cadena20;
begin
  frmABMDepartamento.edtID.Caption := grdDepartamentos.Cells[1, grdDepartamentos.Row];
  frmABMDepartamento.edtNombre.Caption := grdDepartamentos.Cells[2, grdDepartamentos.Row];
  frmABMDepartamento.edtDireccion.Caption := grdDepartamentos.Cells[3, grdDepartamentos.Row];
  frmABMDepartamento.edtCodigoPostal.Caption := grdDepartamentos.Cells[4, grdDepartamentos.Row];
  frmABMDepartamento.cmbPais.Caption := paisId(grdDepartamentos.Cells[5, grdDepartamentos.Row]);
  estado := grdDepartamentos.Cells[6, grdDepartamentos.Row];
  if (estado = 'Habilitado') then
    frmABMDepartamento.cbxEstado.Checked := true
  else if (estado = 'Inhabilitado') then
    frmABMDepartamento.cbxEstado.Checked := false;
end;

procedure TfrmDepartamentos.limpiarCampos();
begin
  frmABMDepartamento.edtNombre.Caption := '';
  frmABMDepartamento.edtDireccion.Caption := '';
  frmABMDepartamento.edtCodigoPostal.Caption := '';
  frmABMDepartamento.cmbPais.Caption := '';
  frmABMDepartamento.cbxEstado.Checked := true
end;

procedure TfrmDepartamentos.edtBuscarChange(Sender: TObject);
begin
  buscarNombre(edtBuscar.Text, vDepartamento, nDepartamento);
  if nDepartamento = 0 then
    begin
      ShowMessage('No se han encontrado resultados: nombres '+edtBuscar.Text+' inexistente');
      edtBuscar.Text := '';
      FormActivate(Sender);
    end
  else
    cargarGrillaDepartamento(vDepartamento, nDepartamento);
end;

procedure TfrmDepartamentos.FormActivate(Sender: TObject);
begin
  cargarVectorD(vDepartamento, nDepartamento);
  cargarGrillaDepartamento(vDepartamento, nDepartamento);
end;

procedure TfrmDepartamentos.FormCreate(Sender: TObject);
begin
 cargarFicheroD(fd);
 cargarVectorD(vDepartamento, nDepartamento);
end;

procedure TfrmDepartamentos.btnHabilitarClick(Sender: TObject);
var
  id: integer;
  pos: LongInt;
  p: TDepartamento;
begin
  id := StrToInt(grdDepartamentos.Cells[1, grdDepartamentos.Row]);
  pos := buscarID(fd, id);
  if (grdDepartamentos.Cells[6, grdDepartamentos.Row] = 'Habilitado') then
    ShowMessage('El Departamento: '+IntToStr(id)+' ya se encuentra habilitado')
  else
    if confirmarOperacion('Esta seguro de modificar el registro?', '' ) then
    begin
      p.id := StrToInt(grdDepartamentos.Cells[1, grdDepartamentos.Row]);
      p.nombre := grdDepartamentos.Cells[2, grdDepartamentos.Row];
      p.direccion := grdDepartamentos.Cells[3, grdDepartamentos.Row];
      p.codigoPostal := grdDepartamentos.Cells[4, grdDepartamentos.Row];
      p.pais.id := StrToInt(paisId(grdDepartamentos.Cells[5, grdDepartamentos.Row])[1]);
      p.estado := 'Habilitado';
      ModificarDepartamento(fd, p, pos);
      ShowMessage('Registro modificado correctamente');
      close;
    end;
end;

procedure TfrmDepartamentos.btnInhabilitarClick(Sender: TObject);
var
  id: integer;
  pos: LongInt;
  p: TDepartamento;
begin
  id := StrToInt(grdDepartamentos.Cells[1, grdDepartamentos.Row]);
  pos := buscarID(fd, id);
  if (grdDepartamentos.Cells[6, grdDepartamentos.Row] = 'Inhabilitado') then
    ShowMessage('El Departamento: '+IntToStr(id)+' ya se encuentra inhabilitado')
  else
    if confirmarOperacion('Esta seguro de modificar el registro?', '' ) then
    begin
      p.id := StrToInt(grdDepartamentos.Cells[1, grdDepartamentos.Row]);
      p.nombre := grdDepartamentos.Cells[2, grdDepartamentos.Row];
      p.direccion := grdDepartamentos.Cells[3, grdDepartamentos.Row];
      p.codigoPostal := grdDepartamentos.Cells[4, grdDepartamentos.Row];
      p.pais.id := StrToInt(paisId(grdDepartamentos.Cells[5, grdDepartamentos.Row])[1]);
      p.estado := 'Inhabilitado';
      ModificarDepartamento(fd, p, pos);
      ShowMessage('Registro modificado correctamente');
      close;
    end;
end;

procedure TfrmDepartamentos.btnAltaClick(Sender: TObject);
begin
  frmABMDepartamento.Show;
  frmABMDepartamento.Caption := 'Alta de Departamentos';
  frmABMDepartamento.lblTitulo.Caption := 'Alta de Departamentos';
  frmABMDepartamento.btnABM.Caption := 'Alta';
  frmABMDepartamento.edtID.Caption := IntToStr(getSiguienteID);
  limpiarCampos();
end;

procedure TfrmDepartamentos.btnModificarClick(Sender: TObject);
begin
 mostrarRegistro();
 frmABMDepartamento.Show;
 frmABMDepartamento.Caption := 'Modificacion de Departamentos';
 frmABMDepartamento.lblTitulo.Caption := 'Modificacion de Departamentos';
 frmABMDepartamento.btnABM.Caption := 'Modificar';
end;

procedure TfrmDepartamentos.btnSalirClick(Sender: TObject);
begin
  close;
end;

end.

