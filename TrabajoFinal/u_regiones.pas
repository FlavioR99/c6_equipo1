unit u_Regiones;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids, u_DAORegiones, u_Estructuras;

type

  { TfrmRegiones }

  TfrmRegiones = class(TForm)
    btnSalir: TButton;
    grdRegiones: TStringGrid;
    lblRegiones: TLabel;
    procedure btnSalirClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
  procedure mostrarVector (v: TVectorRegion ; n : Integer);
  end;

var
  frmRegiones: TfrmRegiones;

implementation

{$R *.lfm}

{ TfrmRegiones }
procedure TfrmRegiones.mostrarVector(v:TVectorRegion ; n: integer);
var
  i,fila : integer;
begin
 grdRegiones.RowCount := 1;
 for i := 1 to n do
   begin
    grdRegiones.RowCount:= grdRegiones.RowCount + 1;
    fila :=  grdRegiones.RowCount - 1;
    grdRegiones.Cells[1,fila] := IntToStr(v[i].id);
    grdRegiones.Cells[2,fila] := v[i].nombre;
   end;
end;

procedure TfrmRegiones.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmRegiones.FormActivate(Sender: TObject);
begin
  mostrarVector(vRegion , nRegion);
end;

procedure TfrmRegiones.FormCreate(Sender: TObject);
begin
  cargarFicheroRegion(f);
  cargarVector(vRegion , nRegion);
end;


end.

