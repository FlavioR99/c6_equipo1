unit u_Empleado;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids,
  u_ABMEmpleados, u_Estructuras, u_DAOEmpleados, u_HistoricoCargos, utiles;

type

  { TfrmEmpleado }

  TfrmEmpleado = class(TForm)
    btnSalir: TButton;
    btnAlta: TButton;
    btnModificar: TButton;
    btnHistoricoCargo: TButton;
    btnHabilitar: TButton;
    btnInhabilitar: TButton;
    edtBuscar: TEdit;
    grdEmpleados: TStringGrid;
    lblBuscar: TLabel;
    lblEmpleados: TLabel;
    procedure btnAltaClick(Sender: TObject);
    procedure btnHabilitarClick(Sender: TObject);
    procedure btnHistoricoCargoClick(Sender: TObject);
    procedure btnInhabilitarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
  procedure mostrarEmpleados(vE: TVectorEmpleado ; n: Integer);
  procedure mostrarRegistro();
  end;

var
  frmEmpleado: TfrmEmpleado;

implementation

{$R *.lfm}

{ TfrmEmpleado }
procedure TfrmEmpleado.mostrarEmpleados(vE: TVectorEmpleado ; n: Integer);
var
  i , fila : Integer;
begin
  grdEmpleados.RowCount := 1;
  for i := 1 to n do
  begin
    grdEmpleados.RowCount := grdEmpleados.RowCount + 1;
    fila := grdEmpleados.RowCount - 1;
    grdEmpleados.Cells[1 , fila] := IntToStr(vE[i].id);
    grdEmpleados.Cells[2 , fila] := vE[i].nombre;
    grdEmpleados.Cells[3 , fila] := vE[i].apellido;
    grdEmpleados.Cells[4 , fila] := vE[i].email;
    grdEmpleados.Cells[5 , fila] := vE[i].telefono;
    grdEmpleados.Cells[6 , fila] := DateTimeToStr(vE[i].fechaDeIngreso);
    grdEmpleados.Cells[7 , fila] := vE[i].cargo.tituloCargo;
    grdEmpleados.Cells[8 , fila] := FloatToStr(vE[i].salario);
    grdEmpleados.Cells[9 , fila] := FloatToStr(vE[i].porcentajeDeComision);
    grdEmpleados.Cells[10 , fila] := vE[i].manager;
    grdEmpleados.Cells[11 , fila] := vE[i].departamento.nombre;
    grdEmpleados.Cells[12 , fila] := vE[i].estado;
  end;
end;

procedure TfrmEmpleado.mostrarRegistro();
var
  estado: cadena20;
begin
  frmABM.edtID.Caption := grdEmpleados.Cells[1, grdEmpleados.Row];
  frmABM.edtNombre.Caption := grdEmpleados.Cells[2, grdEmpleados.Row];
  frmABM.edtApellido.Caption := grdEmpleados.Cells[3, grdEmpleados.Row];
  frmABM.edtEmail.Caption := grdEmpleados.Cells[4, grdEmpleados.Row];
  frmABM.edtTelefono.Caption := grdEmpleados.Cells[5, grdEmpleados.Row];
  frmABM.DateTimePicker1.Date := StrToDateTime(grdEmpleados.Cells[6,grdEmpleados.Row]);
  frmABM.lblFecha.Caption:=grdEmpleados.Cells[6,grdEmpleados.Row];
  frmABM.cmbCargo.Text := grdEmpleados.Cells[7, grdEmpleados.Row];
  frmABM.lblCargoInicial.Caption:= grdEmpleados.Cells[7, grdEmpleados.Row];
  frmABM.edtSalario.Caption := grdEmpleados.Cells[8, grdEmpleados.Row];
  frmABM.edtPorcComision.Caption := grdEmpleados.Cells[9, grdEmpleados.Row];
  frmABM.cmbManager.Text := grdEmpleados.Cells[10, grdEmpleados.Row];
  frmABM.cmbDepto.Text := grdEmpleados.Cells[11, grdEmpleados.Row];
  estado := grdEmpleados.Cells[12, grdEmpleados.Row];
  if (estado = 'Habilitado') then
    frmABM.chkEstado.Checked := True
  else if (estado = 'Inhabilitado') then
    frmABM.chkEstado.Checked := False;

end;

procedure TfrmEmpleado.FormActivate(Sender: TObject);
begin
   cargarVectorEmpleados(vEmpleado , nEmpleado);
   mostrarEmpleados(vEmpleado , nEmpleado);
   frmABM.limpiarCampos();
end;

procedure TfrmEmpleado.FormCreate(Sender: TObject);
begin
   crearFicheroEmpleados(fe);
   cargarVectorEmpleados(vEmpleado , nEmpleado);
end;

procedure TfrmEmpleado.btnAltaClick(Sender: TObject);
begin
  frmABM.limpiarCampos();
  frmABM.btnAceptar.Caption := 'Alta';
  frmABM.lblEmpleados.Caption := 'Alta';
  frmABM.edtID.Text := IntToStr(getSiguienteIdEmpleado);
  frmABM.Show;
end;

procedure TfrmEmpleado.btnModificarClick(Sender: TObject);
begin
  frmABM.btnAceptar.Caption := 'Modificar';
  frmABM.lblEmpleados.Caption := 'Modificar';
  frmABM.Show;
  mostrarRegistro();
end;

procedure TfrmEmpleado.btnHabilitarClick(Sender: TObject);
var
  pos, id: LongInt;
  p: TEmpleado;
begin
  id := StrToInt(grdEmpleados.Cells[1, grdEmpleados.Row]);
  pos := buscarEmpleado(fe, id);
  if (grdEmpleados.Cells[12, grdEmpleados.Row] = 'Habilitado') then
    ShowMessage('El Empleado: '+IntToStr(id)+' ya se encuentra habilitado')
  else
    if confirmarOperacion('Esta seguro de modificar el registro?', '' ) then
    begin
      p.id := StrToInt(grdEmpleados.Cells[1, grdEmpleados.Row]);
      p.nombre := grdEmpleados.Cells[2, grdEmpleados.Row];
      p.apellido:= grdEmpleados.Cells[3, grdEmpleados.Row];
      p.email := grdEmpleados.Cells[4, grdEmpleados.Row];
      p.telefono := grdEmpleados.Cells[5, grdEmpleados.Row];
      p.fechaDeIngreso := StrToDateTime(grdEmpleados.Cells[6, grdEmpleados.Row]);
      p.cargo.tituloCargo := grdEmpleados.Cells[7, grdEmpleados.Row];
      p.salario := StrToFloat(grdEmpleados.Cells[8, grdEmpleados.Row]);
      p.porcentajeDeComision := StrToFloat(grdEmpleados.Cells[9, grdEmpleados.Row]);
      p.manager := grdEmpleados.Cells[10, grdEmpleados.Row];
      p.departamento.nombre := grdEmpleados.Cells[11, grdEmpleados.Row];
      p.estado := 'Habilitado';
      modificarEmpleado(fe, p, pos);
      ShowMessage('Registro modificado correctamente');
      close;
    end;
end;

procedure TfrmEmpleado.btnInhabilitarClick(Sender: TObject);
var
  pos, id: LongInt;
  p: TEmpleado;
begin
  id := StrToInt(grdEmpleados.Cells[1, grdEmpleados.Row]);
  pos := buscarEmpleado(fe, id);
  if (grdEmpleados.Cells[12, grdEmpleados.Row] = 'Inhabilitado') then
    ShowMessage('El Empleado: '+IntToStr(id)+' ya se encuentra inhabilitado')
  else
    if confirmarOperacion('Esta seguro de modificar el registro?', '' ) then
    begin
      p.id := StrToInt(grdEmpleados.Cells[1, grdEmpleados.Row]);
      p.nombre := grdEmpleados.Cells[2, grdEmpleados.Row];
      p.apellido:= grdEmpleados.Cells[3, grdEmpleados.Row];
      p.email := grdEmpleados.Cells[4, grdEmpleados.Row];
      p.telefono := grdEmpleados.Cells[5, grdEmpleados.Row];
      p.fechaDeIngreso := StrToDateTime(grdEmpleados.Cells[6, grdEmpleados.Row]);
      p.cargo.tituloCargo := grdEmpleados.Cells[7, grdEmpleados.Row];
      p.salario := StrToFloat(grdEmpleados.Cells[8, grdEmpleados.Row]);
      p.porcentajeDeComision := StrToFloat(grdEmpleados.Cells[9, grdEmpleados.Row]);
      p.manager := grdEmpleados.Cells[10, grdEmpleados.Row];
      p.departamento.nombre := grdEmpleados.Cells[11, grdEmpleados.Row];
      p.estado := 'Inhabilitado';
      modificarEmpleado(fe, p, pos);
      ShowMessage('Registro modificado correctamente');
      close;
    end;
end;

procedure TfrmEmpleado.btnHistoricoCargoClick(Sender: TObject);
begin
  frmHistoricoCargo.Show;
end;

procedure TfrmEmpleado.btnSalirClick(Sender: TObject);
begin
  close;
end;

end.

