program project1;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, datetimectrls, tachartlazaruspkg, u_Menu, u_Regiones, u_Estructuras,
  u_DAORegiones, u_paises, u_cargos, u_DAOPaises, u_DAOCargos, u_Departamentos,
  u_DAODepartamentos, u_Empleado, u_ABMEmpleados, u_ABMCargos,
  u_ABMDepartamento, u_DAOEmpleados, u_HistoricoCargos, u_DAOHistoricoCargos,
  utiles, u_Consultas
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmRegiones, frmRegiones);
  Application.CreateForm(TfrmPaises, frmPaises);
  Application.CreateForm(TfrmCargos, frmCargos);
  Application.CreateForm(TfrmDepartamentos, frmDepartamentos);
  Application.CreateForm(TfrmEmpleado, frmEmpleado);
  Application.CreateForm(TfrmABM, frmABM);
  Application.CreateForm(TfrmABMCargos, frmABMCargos);
  Application.CreateForm(TfrmABMDepartamento, frmABMDepartamento);
  Application.CreateForm(TfrmHistoricoCargo, frmHistoricoCargo);
  Application.CreateForm(TfrmConsultas, frmConsultas);
  Application.Run;
end.

