unit u_Menu;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  u_Regiones , u_Estructuras , u_DAORegiones , u_paises, u_DAOPaises ,
  u_cargos , u_DAOCargos, u_Departamentos, u_Empleado,u_Consultas;

type

  { TfrmMenu }

  TfrmMenu = class(TForm)
    btnEmpleado: TButton;
    btnConsultas: TButton;
    btnSalir: TButton;
    btnRegiones: TButton;
    btnPaises: TButton;
    btnDepartamentos: TButton;
    btnCargos: TButton;
    lblMenu: TLabel;
    procedure btnCargosClick(Sender: TObject);
    procedure btnConsultasClick(Sender: TObject);
    procedure btnDepartamentosClick(Sender: TObject);
    procedure btnEmpleadoClick(Sender: TObject);
    procedure btnRegionesClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnPaisesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  frmMenu: TfrmMenu;

implementation

{$R *.lfm}

{ TfrmMenu }

procedure TfrmMenu.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmMenu.btnRegionesClick(Sender: TObject);
begin
   frmRegiones.Show;
   //cargarVector(vRegion , nRegion);
end;

procedure TfrmMenu.btnCargosClick(Sender: TObject);
begin
    frmCargos.Show;
end;

procedure TfrmMenu.btnConsultasClick(Sender: TObject);
begin
   frmConsultas.Show;
end;

procedure TfrmMenu.btnDepartamentosClick(Sender: TObject);
begin
  frmDepartamentos.Show;
end;

procedure TfrmMenu.btnEmpleadoClick(Sender: TObject);
begin
  frmEmpleado.Show;
end;

procedure TfrmMenu.btnPaisesClick(Sender: TObject);
begin
   frmPaises.Show;
end;

procedure TfrmMenu.FormCreate(Sender: TObject);
begin
     //cargarFicheroRegion(f);
     crearFicheroP(fp);
     //crearFicheroC(fc);
end;

end.

