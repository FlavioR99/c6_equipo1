unit u_DAODepartamentos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, u_Estructuras, utiles;

procedure crearFicheroD(var fd : TFicheroDepartamentos);
procedure cargarFicheroD(var fd : TFicheroDepartamentos);
procedure cargarVectorD(var v:TVectorDepartamento; var n:integer);
function getSiguienteID():integer;
procedure NuevoDepartamento(var fd: TFicheroDepartamentos; pd: TDepartamento);
function buscarID(var fd: TFicheroDepartamentos; id: integer):longInt;
procedure ModificarDepartamento(var fd: TFicheroDepartamentos; pd: TDepartamento; posicion: longInt);
function validacionNombre(var fd: TFicheroDepartamentos; nombre: cadena30):boolean;
procedure buscar(var v: TVectorDepartamento; var n: integer);
procedure buscarNombre(nombre: cadena30; var v: TVectorDepartamento; var n: integer);
function IdPais (d: integer): cadena50;
function paisId (d: cadena50): cadena50;

implementation

function paisId (d: cadena50): cadena50;
var
  pais: cadena50;
begin
  case d of
  'Italia': pais := '1 - Italia';
  'Japon': pais := '2 - Japon';
  'Estados Unidos': pais := '3 - Estados Unidos';
  'Canada': pais := '4 - Canada';
  'Egipto': pais := '5 - Egipto';
  'Argentina': pais := '6 - Argentina';
  end;
  paisId := pais;
end;

function IdPais (d: integer): cadena50;
var
  pais: cadena50;
begin
  case d of
  1: pais := 'Italia';
  2: pais := 'Japon';
  3: pais := 'Estados Unidos';
  4: pais := 'Canada';
  5: pais := 'Egipto';
  6: pais := 'Argentina';
  end;
  IdPais := pais;
end;

procedure buscarNombre(nombre: cadena30; var v: TVectorDepartamento; var n: integer);
var
  dep: TDepartamento;
begin
  try
    reset(fd);
    n := 0;
    while not eof(fd) do
    begin
      read(fd, dep);
       if (like(nombre, dep.nombre))or (nombre = '') then
        begin
          inc(n);
          v[n] := dep;
        end;
    end;
  finally
    closeFile(fd);
  end;
end;

procedure buscar(var v: TVectorDepartamento; var n: integer);
var
  dep: TDepartamento;
begin
 try
     reset(fd);
     n:=0;
     while not eof(fd) do
      begin
        read(fd, dep);
          inc(n);
          v[n]:= dep;
      end;
 finally
 closefile(fd);
 end;
end;

function validacionNombre(var fd: TFicheroDepartamentos; nombre: cadena30):boolean;
var
  encontrado: boolean;
  pd: TDepartamento;
begin
 try
  reset(fd);
  encontrado := false;
  while not eof(fd) and (encontrado = false) do
     begin
       read(fd,pd);
       if nombre = pd.nombre then
         encontrado := true;
     end;
  CloseFile(fd);
 finally
   validacionNombre := encontrado;
 end;
end;

function buscarID(var fd: TFicheroDepartamentos; id: integer):longInt;
var
 i, posicion: longInt;
 pd: TDepartamento;
begin
 try
  reset(fd);
  posicion := NO_ENCONTRADO;
  i := 0;
  while not eof(fd) and (posicion = NO_ENCONTRADO) do
     begin
       read(fd,pd);
       if id = pd.id then
         posicion := i
       else
         i := i + 1;
     end;
  CloseFile(fd);
 finally
   buscarID := posicion;
 end;
end;

procedure NuevoDepartamento(var fd: TFicheroDepartamentos; pd: TDepartamento);
begin
 try
  reset(fd);
  seek(fd, FileSize(fd));
  write(fd,pd);
 finally
   CloseFile(fd);
 end;
end;

procedure ModificarDepartamento(var fd: TFicheroDepartamentos; pd: TDepartamento; posicion: longInt);
begin
 try
  reset(fd);
  seek(fd, posicion);
  write(fd, pd);
 finally
   CloseFile(fd);
 end;
end;

function getSiguienteID():integer;
var
 id: integer;
 pd: TDepartamento;
begin
 id := 0;
 reset(fd);
 while not eof(fd) do
    begin
      read(fd, pd);
      inc(id);
    end;
 CloseFile(fd);
 getSiguienteID := id + 1;
end;

procedure crearFicheroD(var fd:TFicheroDepartamentos);
begin
  assignFile(fd,FICHERO_DEPARTAMENTOS);
  try
    reset(fd);
  except
    rewrite(fd);
    closefile(fd);
  end;
end;

procedure cargarFicheroD(var fd : TFicheroDepartamentos);
var
  pd : TDepartamento;
begin
  crearFicheroD(fd);
  reset(fd);
  pd.id := 1;
  pd.nombre := 'Administracion';
  pd.direccion := 'Via Cola di Rie 1297';
  pd.codigoPostal := '00989';
  pd.pais.id := 1;
  pd.estado := 'Habilitado';
  write(fd,pd);
  pd.id := 2;
  pd.nombre := 'Marketing';
  pd.direccion := 'Calle della Testa 93091';
  pd.codigoPostal := '10934';
  pd.pais.id := 1;
  pd.estado := 'Habilitado';
  write(fd,pd);
  pd.id := 3;
  pd.nombre := 'Compras';
  pd.direccion := 'Shinjuku-ku 2017';
  pd.codigoPostal := '1689';
  pd.pais.id := 2;
  pd.estado := 'Habilitado';
  write(fd,pd);
  pd.id := 4;
  pd.nombre := 'Recursos Humanos';
  pd.direccion := 'Kamiya-cho 9450';
  pd.codigoPostal := '6823';
  pd.pais.id := 2;
  pd.estado := 'Habilitado';
  write(fd,pd);
  pd.id := 5;
  pd.nombre := 'IT';
  pd.direccion := 'Reconquista 656 piso 3A';
  pd.codigoPostal := '1003';
  pd.pais.id := 6;
  pd.estado := 'Habilitado';
  write(fd,pd);
  closeFile(fd);
end;

procedure cargarVectorD(var v:TVectorDepartamento; var n:integer);
var
  fila : integer;
  pd : TDepartamento;
begin
   try
    reset(fd);
    fila := 0;
    n := 0;
    while not eof(fd) do
       begin
        read(fd,pd);
          begin
            fila := fila + 1;
            vDepartamento[fila].id := pd.id;
            vDepartamento[fila].nombre := pd.nombre;
            vDepartamento[fila].direccion := pd.direccion;
            vDepartamento[fila].codigoPostal := pd.codigoPostal;
            vDepartamento[fila].pais.id := pd.pais.id;
            vDepartamento[fila].estado := pd.estado;
          end;
       end;
    n := fila;
    closeFile(fd);
    except
      on E: EInOutError do
      ShowMessage('Error: '+E.ClassName+'/'+E.Message);
  end;
end;

end.

