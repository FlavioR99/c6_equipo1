unit u_cargos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids,
  u_DAOCargos , u_Estructuras , u_ABMCargos, utiles;

type

  { TfrmCargos }

  TfrmCargos = class(TForm)
    btnSalir: TButton;
    btnNuevo: TButton;
    btnModificar: TButton;
    btnHabilitar: TButton;
    btnInhabilitar: TButton;
    edtBuscar: TEdit;
    grdCargos: TStringGrid;
    lblBuscar: TLabel;
    lblCargos: TLabel;
    procedure btnHabilitarClick(Sender: TObject);
    procedure btnInhabilitarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure btnNuevoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
  procedure mostrarVectorC (v: TVectorCargo ; n : Integer ; buscado: String);
  procedure mostrarRegistro();
  end;

var
  frmCargos: TfrmCargos;

implementation

{$R *.lfm}

{ TfrmCargos }

procedure TfrmCargos.mostrarVectorC(v:TVectorCargo ; n: integer ; buscado:String);
var
  i,fila : integer;
begin
 grdCargos.RowCount := 1;
 for i := 1 to n do
   begin
   if(buscado = '') or (buscado = v[i].tituloCargo) then
   begin
      grdCargos.RowCount:= grdCargos.RowCount + 1;
      fila :=  grdCargos.RowCount - 1;
      grdCargos.Cells[1,fila] := IntToStr(v[i].id);
      grdCargos.Cells[2,fila] := v[i].abreviatura;
      grdCargos.Cells[3,fila] := v[i].tituloCargo;
      grdCargos.Cells[4,fila] := FloatToStr(v[i].salarioMin);
      grdCargos.Cells[5,fila] := FloatToStr(v[i].salarioMax);
      grdCargos.Cells[6,fila] := v[i].estado;
    end;
   end;
end;

procedure TfrmCargos.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmCargos.btnNuevoClick(Sender: TObject);
begin
  frmABMCargos.Show;
  frmABMCargos.lblTitulo.Caption:= 'Nuevo';
  frmABMCargos.btnAceptar.Caption:='Nuevo';
  frmABMCargos.inicializarComponentesC();
end;

procedure TfrmCargos.btnModificarClick(Sender: TObject);
begin
  mostrarRegistro();
  frmABMCargos.Show;
  frmABMCargos.lblTitulo.Caption:= 'Modificar';
  frmABMCargos.btnAceptar.Caption:='Guardar';
  frmABMCargos.edtAbreviatura.Enabled:=False;
end;

procedure TfrmCargos.btnHabilitarClick(Sender: TObject);
var
  pos, id: LongInt;
  p: TCargo;
begin
  id := StrToInt(grdCargos.Cells[1, grdCargos.Row]);
  pos := buscarCargo(fc, id);
  if (grdCargos.Cells[6, grdCargos.Row] = 'Habilitado') then
    ShowMessage('El Cargo: '+IntToStr(id)+' ya se encuentra habilitado')
  else
    if confirmarOperacion('Esta seguro de modificar el registro?', '' ) then
    begin
      p.id := StrToInt(grdCargos.Cells[1, grdCargos.Row]);
      p.abreviatura := grdCargos.Cells[2, grdCargos.Row];
      p.tituloCargo:= grdCargos.Cells[3, grdCargos.Row];
      p.salarioMin := StrToFloat(grdCargos.Cells[4, grdCargos.Row]);
      p.salarioMax := StrToFloat(grdCargos.Cells[5, grdCargos.Row]);
      p.estado := 'Habilitado';
      ModificarCargo(fc, p, pos);
      ShowMessage('Registro modificado correctamente');
      close;
    end;
end;

procedure TfrmCargos.btnInhabilitarClick(Sender: TObject);
var
  pos, id: LongInt;
  p: TCargo;
begin
  id := StrToInt(grdCargos.Cells[1, grdCargos.Row]);
  pos := buscarCargo(fc, id);
  if (grdCargos.Cells[6, grdCargos.Row] = 'Inhabilitado') then
    ShowMessage('El Cargo: '+IntToStr(id)+' ya se encuentra inhabilitado')
  else
    if confirmarOperacion('Esta seguro de modificar el registro?', '' ) then
    begin
      p.id := StrToInt(grdCargos.Cells[1, grdCargos.Row]);
      p.abreviatura := grdCargos.Cells[2, grdCargos.Row];
      p.tituloCargo:= grdCargos.Cells[3, grdCargos.Row];
      p.salarioMin := StrToFloat(grdCargos.Cells[4, grdCargos.Row]);
      p.salarioMax := StrToFloat(grdCargos.Cells[5, grdCargos.Row]);
      p.estado := 'Inhabilitado';
      ModificarCargo(fc, p, pos);
      ShowMessage('Registro modificado correctamente');
      close;
    end;
end;

procedure TfrmCargos.FormActivate(Sender: TObject);
begin
  cargarVectorC(vCargo,nCargo);
  mostrarVectorC(vCargo , nCargo, '');

end;

procedure TfrmCargos.FormCreate(Sender: TObject);
begin
    cargarFicheroCargo(fc);
    cargarVectorC(vCargo,nCargo);
end;

procedure TfrmCargos.mostrarRegistro();
var
  estado: cadena20;
begin
  frmABMCargos.edtID.Caption := grdCargos.Cells[1, grdCargos.Row];
  frmABMCargos.edtAbreviatura.Caption := grdCargos.Cells[2, grdCargos.Row];
  frmABMCargos.edtTituloCargo.Caption := grdCargos.Cells[3, grdCargos.Row];
  frmABMCargos.edtMin.Caption := grdCargos.Cells[4, grdCargos.Row];
  frmABMCargos.edtMax.Caption := grdCargos.Cells[5, grdCargos.Row];
  estado := grdCargos.Cells[6, grdCargos.Row];
  if (estado = 'Habilitado') then
    frmABMCargos.cbxEstado.Checked := True
  else if (estado = 'Inhabilitado') then
    frmABMCargos.cbxEstado.Checked := False;

end;

end.

