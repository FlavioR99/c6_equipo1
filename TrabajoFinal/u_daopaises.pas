unit u_DAOPaises;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,u_Estructuras,Dialogs;

procedure crearFicheroP(var fp:TFicheroPaises);
procedure cargarFicheroPais(var fp:TFicheroPaises);
procedure cargarVector(var v:tVectorPais;var n:integer);


implementation
procedure crearFicheroP(var fp:TFicheroPaises);
begin
  assignFile(fp,FICHERO_PAISES);
  try
    reset(fp);
    closeFile(fp);
  except
    rewrite(fp);
    closeFile(fp);
  end;
end;

procedure cargarFicheroPais(var fp:TFicheroPaises);
var
  pp : TPais;
begin
  crearFicheroP(fp);
  assignFile(fp,FICHERO_PAISES);
  reset(fp);


  pp.id := 1;
  pp.abreviatura:= 'IT';
  pp.nombre := 'Italia';
  pp.region.id:= 1;
  write(fp,pp);

  pp.id := 2;
  pp.abreviatura:= 'JP';
  pp.nombre := 'Japón';
  pp.region.id:= 3;
  write(fp,pp);

  pp.id := 3;
  pp.abreviatura:= 'US';
  pp.nombre := 'Estados Unidos';
  pp.region.id:= 2;
  write(fp,pp);

  pp.id := 4;
  pp.abreviatura:= 'CA';
  pp.nombre := 'Canadá';
  pp.region.id:= 2;
  write(fp,pp);

  pp.id := 5;
  pp.abreviatura:= 'EG';
  pp.nombre := 'Egipto';
  pp.region.id:= 4;
  write(fp,pp);

  pp.id := 6;
  pp.abreviatura:= 'AR';
  pp.nombre := 'Argentina';
  pp.region.id:= 2;
  write(fp,pp);

  closeFile(fp);
end;


procedure cargarVector(var v:tVectorPais;var n:integer);
var
  fila : integer;
  R : TPais;
begin
  cargarFicheroPais(fp);
   try
    assignFile(fp,FICHERO_PAISES);
    reset(fp);
    fila := 0;
    N := 0;
    while not eof(fp) do
       begin
        read(fp,R);
          begin
            fila := fila + 1;
            v[fila].id:= R.id;
            v[fila].abreviatura := R.abreviatura;
            v[fila].nombre:= R.nombre;
            v[fila].region.id := R.region.id;
          end;
       end;
    n := fila;
    closeFile(fp);
    except
      on E: EInOutError do
      ShowMessage('Error: '+E.ClassName+'/'+E.Message)
  end;
end;


end.
