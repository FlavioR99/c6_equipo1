unit u_Consultas;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, Grids,
  StdCtrls, TAGraph, TASeries, TASources, u_Estructuras, u_DAOEmpleados, u_DAOHistoricoCargos;

type

  { TfrmConsultas }

  TfrmConsultas = class(TForm)
    btnGrafico: TButton;
    btnSalir: TButton;
    btnExportar: TButton;
    Button1: TButton;
    Chart1: TChart;
    Chart1PieSeries1: TPieSeries;
    cmbDepto: TComboBox;
    ComboBox1: TComboBox;
    grdEmpleados: TStringGrid;
    lblMostrarP: TLabel;
    lblCont: TLabel;
    lblPromedio: TLabel;
    lblTitulo: TLabel;
    ListChartSource1: TListChartSource;
    MemoManager: TMemo;
    memoEmpleado: TMemo;
    SaveDialog1: TSaveDialog;
    SaveDialog2: TSaveDialog;
    grdEmpleadosCambioCargo: TStringGrid;
    TabSheet4: TTabSheet;
    tsP1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    procedure btnExportarClick(Sender: TObject);
    procedure btnGraficoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cmbDeptoChange(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure tsP1Change(Sender: TObject);
  private

  public
  procedure mostrarEmpleados(vE: TVectorEmpleado ; n: Integer);
  procedure mostrarEmpleados2(vE: TVectorEmpleado ; n: Integer);
  procedure mostrarEmpleados3(vCE: TVectorConsultaEmplado; nCE: Integer);
  end;

var
  frmConsultas: TfrmConsultas;
  contarMemo,contarMemo2: Integer;

implementation

{$R *.lfm}



{ TfrmConsultas }
procedure TfrmConsultas.mostrarEmpleados(vE: TVectorEmpleado ; n: Integer);
var
  i , fila : Integer;
begin
  grdEmpleados.RowCount := 1;
  for i := 1 to n do
  begin
    if(vE[i].salario > StrToFloat(lblPromedio.Caption))then
    begin
      grdEmpleados.RowCount := grdEmpleados.RowCount + 1;
      fila := grdEmpleados.RowCount - 1;
      grdEmpleados.Cells[1 , fila] := IntToStr(vE[i].id);
      grdEmpleados.Cells[2 , fila] := vE[i].nombre;
      grdEmpleados.Cells[3 , fila] := vE[i].apellido;
      grdEmpleados.Cells[4 , fila] := vE[i].email;
      grdEmpleados.Cells[5 , fila] := vE[i].telefono;
      grdEmpleados.Cells[6 , fila] := DateTimeToStr(vE[i].fechaDeIngreso);
      grdEmpleados.Cells[7 , fila] := vE[i].cargo.tituloCargo;
      grdEmpleados.Cells[8 , fila] := FloatToStr(vE[i].salario);
      grdEmpleados.Cells[9 , fila] := FloatToStr(vE[i].porcentajeDeComision);
      grdEmpleados.Cells[10 , fila] := vE[i].manager;
      grdEmpleados.Cells[11 , fila] := vE[i].departamento.nombre;
      grdEmpleados.Cells[12 , fila] := vE[i].estado;
      contarMemo:=contarMemo+1;
    end;
  end;
end;

procedure TfrmConsultas.mostrarEmpleados2(vE: TVectorEmpleado ; n: Integer);
var
  i , fila : Integer;
begin
  grdEmpleados.RowCount := 1;
  for i := 1 to n do
  begin
    grdEmpleados.RowCount := grdEmpleados.RowCount + 1;
    fila := grdEmpleados.RowCount - 1;
    grdEmpleados.Cells[1 , fila] := IntToStr(vE[i].id);
    grdEmpleados.Cells[2 , fila] := vE[i].nombre;
    grdEmpleados.Cells[3 , fila] := vE[i].apellido;
    grdEmpleados.Cells[4 , fila] := vE[i].email;
    grdEmpleados.Cells[5 , fila] := vE[i].telefono;
    grdEmpleados.Cells[6 , fila] := DateTimeToStr(vE[i].fechaDeIngreso);
    grdEmpleados.Cells[7 , fila] := vE[i].cargo.tituloCargo;
    grdEmpleados.Cells[8 , fila] := FloatToStr(vE[i].salario);
    grdEmpleados.Cells[9 , fila] := FloatToStr(vE[i].porcentajeDeComision);
    grdEmpleados.Cells[10 , fila] := vE[i].manager;
    grdEmpleados.Cells[11 , fila] := vE[i].departamento.nombre;
    grdEmpleados.Cells[12 , fila] := vE[i].estado;
    contarMemo2:=contarMemo2+1;
  end;
end;
procedure TfrmConsultas.mostrarEmpleados3(vCE: TVectorConsultaEmplado; nCE: Integer);
var
  i, fila: Integer;
begin
  grdEmpleadosCambioCargo.RowCount:= 1;
  for i:= 1 to nCE do
  begin
    grdEmpleadosCambioCargo.RowCount:= grdEmpleadosCambioCargo.RowCount + 1;
    fila:= grdEmpleadosCambioCargo.RowCount - 1;
    grdEmpleadosCambioCargo.Cells[1 , fila] := vCE[i].empleado.nombre + ' ' + vCE[i].empleado.apellido;
    grdEmpleadosCambioCargo.Cells[2 , fila] := IntToStr(vCE[i].N);
  end;
end;


procedure TfrmConsultas.btnSalirClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmConsultas.Button1Click(Sender: TObject);
var
  i: Integer;
  aux: array [1..600] of Char;
begin
  for i:=1 to contarMemo2 do
  begin
  aux:= grdEmpleados.Cells[1,i] + ';' + grdEmpleados.Cells[2,i] + ';' + grdEmpleados.Cells[3,i] + ';' +
  grdEmpleados.Cells[4,i] + ';' + grdEmpleados.Cells[5,i] + ';' + grdEmpleados.Cells[6,i] + ';' +
  grdEmpleados.Cells[7,i] + ';' + grdEmpleados.Cells[8,i] + ';' + grdEmpleados.Cells[9,i] + ';' +
  grdEmpleados.Cells[10,i] + ';' + grdEmpleados.Cells[11,i] + ';' + grdEmpleados.Cells[12,i] ;
    MemoManager.Lines.Add(aux);
  end;
  SaveDialog2.Execute;
  memoManager.Lines.SaveToFile(SaveDialog2.FileName);
end;

procedure TfrmConsultas.btnExportarClick(Sender: TObject);
var
  i: Integer;
  aux: array [1..600] of Char;
begin
  for i:=1 to contarMemo do
  begin
  aux:= grdEmpleados.Cells[1,i] + ';' + grdEmpleados.Cells[2,i] + ';' + grdEmpleados.Cells[3,i] + ';' +
  grdEmpleados.Cells[4,i] + ';' + grdEmpleados.Cells[5,i] + ';' + grdEmpleados.Cells[6,i] + ';' +
  grdEmpleados.Cells[7,i] + ';' + grdEmpleados.Cells[8,i] + ';' + grdEmpleados.Cells[9,i] + ';' +
  grdEmpleados.Cells[10,i] + ';' + grdEmpleados.Cells[11,i] + ';' + grdEmpleados.Cells[12,i] ;
    memoEmpleado.Lines.Add(aux);
  end;
  SaveDialog1.Execute;
  memoEmpleado.Lines.SaveToFile(SaveDialog1.FileName);
end;

procedure TfrmConsultas.btnGraficoClick(Sender: TObject);
var
  i: Byte;
  valor: Double;
begin
  grdEmpleados.Visible := false;
  for i := 0 to Chart1.SeriesCount -1 do
  begin
    if (Chart1.Series[i] is TPieSeries) then
       (Chart1.Series[i] as TPieSeries).Clear;
  end;
  ListChartSource1.Clear;
  for i := 1 to 4 do
  begin

    valor := paises(vEmpleado, i);
    case i of
    1:ListChartSource1.AddXYList(I, valor, 'Europa', RGBToColor( random(255), random(255), random(255 )) );

    2:ListChartSource1.AddXYList(I, valor, 'America', RGBToColor( random(255), random(255), random(255 )) );

    3:ListChartSource1.AddXYList(I, valor, 'Asia', RGBToColor( random(255), random(255), random(255 )) );

    4:ListChartSource1.AddXYList(I, valor, 'Este medio y Africa', RGBToColor( random(255), random(255), random(255 )) );

    end;

  end;
end;

procedure TfrmConsultas.cmbDeptoChange(Sender: TObject);
begin
   cargarVectorEmpleadosProm(vEmpleado , nEmpleado, cmbDepto.Caption);
   if(cont>0)then
   begin
     lblCont.caption:=IntToStr(cont);
     lblPromedio.Caption := floatToStr((SumarSalarios(fe,cmbDepto.Caption))/cont);
   end;
   mostrarEmpleados(vEmpleado , nEmpleado);

end;

procedure TfrmConsultas.ComboBox1Change(Sender: TObject);
begin
   cargarVectorEmpleadosManager(vEmpleado , nEmpleado, ComboBox1.Caption);
   mostrarEmpleados2(vEmpleado , nEmpleado);
end;

procedure TfrmConsultas.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  memoEmpleado.Lines.Clear;
  with cmbDepto do
  begin
    Items.BeginUpdate;
    Items.Clear;
    for i:= 1 to nDepartamento do
        Items.Add(vDepartamento[i].nombre);
    Items.EndUpdate;
  end;
  with ComboBox1 do
  begin
    Items.BeginUpdate;
    Items.Clear;
    for i:= 1 to nEmpleado do
        Items.Add(vEmpleado[i].apellido);
    Items.EndUpdate;
  end;
end;

procedure TfrmConsultas.TabSheet1Show(Sender: TObject);
begin
    grdEmpleados.Visible:=true;
end;

procedure TfrmConsultas.TabSheet2Show(Sender: TObject);
begin
    grdEmpleados.Visible:=true;
end;

procedure TfrmConsultas.TabSheet3Show(Sender: TObject);
begin
    grdEmpleados.Visible:=false;
    cargarVectorConsultaEmp(vHistoricoCargo, nHistoricoCargo);
    mostrarEmpleados3(vConsultaEmpleado , nConsultaEmpleado);
end;

procedure TfrmConsultas.TabSheet4Show(Sender: TObject);
begin
    grdEmpleados.Visible:=false;
end;

procedure TfrmConsultas.tsP1Change(Sender: TObject);
begin
  grdEmpleados.Clean();
  grdEmpleados.Cells[1,0]:= 'Id';
  grdEmpleados.Cells[2,0]:= 'Nombre';
  grdEmpleados.Cells[3,0]:= 'Apellido';
  grdEmpleados.Cells[4,0]:= 'E-Mail';
  grdEmpleados.Cells[5,0]:= 'Telefono';
  grdEmpleados.Cells[6,0]:= 'Fecha Ingreso';
  grdEmpleados.Cells[7,0]:= 'Cargo';
  grdEmpleados.Cells[8,0]:= 'Salario';
  grdEmpleados.Cells[9,0]:= '% C';
  grdEmpleados.Cells[10,0]:= 'Manager';
  grdEmpleados.Cells[11,0]:= 'Departamento';
  grdEmpleados.Cells[12,0]:= 'Estado';
end;

end.

