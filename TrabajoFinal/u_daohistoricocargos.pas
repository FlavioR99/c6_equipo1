unit u_DAOHistoricoCargos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, u_Estructuras, Dialogs;

 procedure crearFicheroHistCargos(var fhc: TFicheroHistoricoCargos);
 procedure altaHistCargos(var fhc: TFicheroHistoricoCargos ; histCargos: THistoricoCargo);
 procedure modificarHistCargos(var fhc: TFicheroHistoricoCargos ; histCargos: THistoricoCargo ; posicion: LongInt);
 procedure cargarVectorHistCargos(var vHC: TVectorHistoricoCargos ; var nHC: Integer);
 function buscarHistCargos(var fhc: TFicheroHistoricoCargos ; ID: INTEGER): LongInt;
 function existeEmp (var vHC: TVectorHistoricoCargos; var nHC: Integer; var vCE : TVectorConsultaEmplado; var nCE: Integer): boolean;
 procedure cargarVectorConsultaEmp(var vHC: TVectorHistoricoCargos; var nHC: Integer);
implementation

function buscarHistCargos(var fhc: TFicheroHistoricoCargos ; ID: INTEGER): LongInt;
var
   i, posicion: LongInt;
   hist: THistoricoCargo;
begin
   try
     reset(fhc);
     posicion:= NO_ENCONTRADO;
     i:= 0;
     while (not EOF(fhc)) and (posicion = NO_ENCONTRADO) do
     begin
       read(fhc , hist);
       if (hist.empleado.id = ID)and((hist.fechaFin = '-')or(hist.fechaFin = '')) then
          posicion:= i;
       i:= i + 1;
     end;
     CloseFile(fhc);
   finally
     buscarHistCargos:= posicion;
   end;
end;

procedure altaHistCargos(var fhc: TFicheroHistoricoCargos ;  histCargos: THistoricoCargo);
begin
  try
    Reset(fhc);
    Seek(fhc , FileSize(fhc));
    Write(fhc , histCargos);
  finally
    CloseFile(fhc);
  end;
end;

procedure crearFicheroHistCargos(var fhc: TFicheroHistoricoCargos);
begin
  AssignFile(fhc , FICHERO_HISTORICO_CARGOS);
  try
    Reset(fhc);
  except
    Rewrite(fhc);
    CloseFile(fhc);
  end;
end;

procedure modificarHistCargos(var fhc: TFicheroHistoricoCargos ; histCargos: THistoricoCargo ; posicion: LongInt);
begin
  try
    Reset(fhc);
    Seek(fhc , posicion);
    Write(fhc , histCargos);
  finally
    CloseFile(fhc);
  end;
end;

procedure cargarVectorHistCargos(var vHC: TVectorHistoricoCargos ; var nHC: Integer);
var
   fila: Integer;
   histCargos: THistoricoCargo;
begin
  try
    reset(fhc);
    fila:= 0;
    nHC:= 0;
    while not EOF(fhc) do
    begin
      Read(fhc , histCargos);
      begin
        fila:= fila + 1;
        vHC[fila].departamento:= histCargos.departamento;
        vHC[fila].cargo:= histCargos.cargo;
        vHC[fila].empleado:= histCargos.empleado;
        vHC[fila].fechaInicio:= histCargos.fechaInicio;
        vHC[fila].fechaFin:= histCargos.fechaFin;
        vHC[fila].id:= histCargos.id;
      end;
    end;
    nHC:= fila;
    CloseFile(fhc);
  except
    on E: EInOutError do
    ShowMessage('Error');
  end;
end;
function existeEmp (var vHC: TVectorHistoricoCargos; var nHC: Integer; var vCE : TVectorConsultaEmplado; var nCE: Integer): boolean;
var
   i, j: Integer;
   flag: Boolean;
begin
  i:= 1;
  j:= 1;
  flag:= false;
  while (i <= nHC) and (flag = false) do
  begin
     while (j <= nCE) and (flag = false) do
     begin
        if (vCE[j].empleado.nombre = vHC[i].empleado.nombre) then
           flag := true
        else j:= j+1;
     end;
     i:= i + 1;
  end;
  existeEmp := flag;
end;
function cantidadCargos (nombre: String ; var vHC: TVectorHistoricoCargos; var nHC: Integer): Integer;
var
  i , contador: Integer;
begin
  contador:= 0;
  for i:= 1 to nHC do
  begin
    if nombre = vHC[i].empleado.nombre then
       contador:= contador + 1;
  end;
  cantidadCargos := contador;
end;
procedure cargarVectorConsultaEmp(var vHC: TVectorHistoricoCargos; var nHC: Integer);
var
  i, fila: Integer;
begin
  fila:= 0;
  for i:= 1 to nHC do
  begin
    if (not existeEmp(vHC, nHC , vConsultaEmpleado , nConsultaEmpleado)) then
    begin
      fila:= fila + 1;
      vConsultaEmpleado[fila].empleado:= vHC[i].empleado;
      vConsultaEmpleado[fila].N:= cantidadCargos(vHC[i].empleado.nombre, vHC, nHC);
    end;
  end;
  nConsultaEmpleado:= fila;
end;

end.

