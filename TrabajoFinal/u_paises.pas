unit u_paises;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids,u_DAOPaises, u_Estructuras;

type

  { TfrmPaises }

  TfrmPaises = class(TForm)
    btnSalir: TButton;
    btnBuscar: TButton;
    edtBuscar: TEdit;
    grdPaises: TStringGrid;
    Label1: TLabel;
    lblPaises: TLabel;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
  procedure mostrarVector (v: tVectorPais ; n : Integer ; buscado: String);
  end;

var
  frmPaises: TfrmPaises;

implementation

{$R *.lfm}

{ TfrmPaises }

procedure TfrmPaises.mostrarVector(v:tVectorPais ; n: integer ; buscado:String);
var
  i,j,k,fila : integer;
  tam1,tam2 : Integer;
begin
 tam1 := Length(buscado);
 grdPaises.RowCount := 1;
 for i := 1 to n do
   begin
   tam2 := Length(v[i].nombre);
   if(tam2 >= tam1)then
     begin
       k := (tam2-tam1)+1;
       if(buscado = '') then
         begin
            grdPaises.RowCount:= grdPaises.RowCount + 1;
            fila :=  grdPaises.RowCount - 1;
            grdPaises.Cells[1,fila] := IntToStr(v[i].id);
            grdPaises.Cells[2,fila] := v[i].abreviatura;
            grdPaises.Cells[3,fila] := v[i].nombre;
            grdPaises.Cells[4,fila] := IntToStr(v[i].region.id);
         end
       else
         begin
          for j:= 1 to k do
           begin
             if (UpperCase(buscado)  = UpperCase(v[i].nombre[j..tam1+j-1])) then
               begin
                  grdPaises.RowCount:= grdPaises.RowCount + 1;
                  fila :=  grdPaises.RowCount - 1;
                  grdPaises.Cells[1,fila] := IntToStr(v[i].id);
                  grdPaises.Cells[2,fila] := v[i].abreviatura;
                  grdPaises.Cells[3,fila] := v[i].nombre;
                  grdPaises.Cells[4,fila] := IntToStr(v[i].region.id);
                  break;
                end;
           end;
         end;
     end;
   end;
end;

procedure TfrmPaises.btnSalirClick(Sender: TObject);
begin
  close;
end;

procedure TfrmPaises.FormActivate(Sender: TObject);
begin
  mostrarVector(vPais , nPais, '');
end;

procedure TfrmPaises.FormCreate(Sender: TObject);
begin
  cargarFicheroPais(fp);
  cargarVector(vPais,nPais);
end;

procedure TfrmPaises.btnBuscarClick(Sender: TObject);
begin
   mostrarVector(vPais , nPais, edtBuscar.Text);
end;


end.

