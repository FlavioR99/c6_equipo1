unit u_Estructuras;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;
const
  PATH_FICHEROS    = 'ficheros/';
  FICHERO_REGIONES = PATH_FICHEROS + 'regiones.dat';
  FICHERO_AUXILIAR_REGIONES = PATH_FICHEROS +'auxiliar.dat';
  FICHERO_PAISES = PATH_FICHEROS + 'paises.dat';
  FICHERO_AUXILIAR_PAISES = PATH_FICHEROS +'auxiliar2.dat';
  FICHERO_CARGOS = PATH_FICHEROS + 'cargos.dat';
  FICHERO_AUXILIAR_CARGOS = PATH_FICHEROS +'auxiliar3.dat';
  FICHERO_DEPARTAMENTOS = PATH_FICHEROS + 'departamentos.dat';
  FICHERO_AUXILIAR_DEPARTAMENTOS = PATH_FICHEROS + 'auxiliar4.dat';
  FICHERO_EMPLEADOS = PATH_FICHEROS + 'empleados.dat';
  FICHERO_AUXILIAR_EMPLEADOS = PATH_FICHEROS + 'auxiliar5.dat';
  FICHERO_HISTORICO_CARGOS = PATH_FICHEROS + 'HistoricoCargos.dat';
  FICHERO_AUXILIAR_HISTORICO_CARGOS = PATH_FICHEROS + 'auxiliar6.dat';
  NO_ENCONTRADO = -1;

type
  cadena20 = String[20];
  cadena2 = String[2];
  cadena50 = String[50];
  cadena10 = String[10];
  cadena30 = String[30];

  TRegion = record
    id : Integer;
    nombre : cadena20;
  end;
  TPais = record
    id : Integer;
    abreviatura : cadena2;
    nombre : cadena50;
    region : TRegion;
  end;
  TCargo = record
      id: Integer;
      abreviatura : cadena10;
      tituloCargo : cadena30;
      salarioMin : Real;
      salarioMax : Real;
      estado : cadena20;
  end;
  TDepartamento = record
      id : Integer;
      nombre : cadena30;
      direccion : cadena30;
      codigoPostal : cadena10;
      pais : TPais;
      estado : cadena20;
  end;
  TEmpleado = record
      id: integer;
      nombre: cadena50;
      apellido: cadena50;
      email: cadena50;
      telefono: cadena20;
      fechaDeIngreso: TDate;
      cargo: TCargo;
      salario: real;
      porcentajeDeComision: real;
      manager: cadena50;
      departamento: TDepartamento;
      estado: cadena20;
  end;
  THistoricoCargo = record
      id: Integer;
      empleado : TEmpleado;
      fechaInicio: TDate;
      fechaFin: cadena10;
      cargo : TCargo;
      departamento : TDepartamento;
  end;
  TConsultaEmpleado= record
      N: Integer;
      empleado: TEmpleado;
  end;

  TFicheroRegiones = file of TRegion;
  TFicheroPaises = file of TPais;
  TFicheroCargos = file of TCargo;
  TFicheroDepartamentos = file of TDepartamento;
  TFicheroEmpleado = file of TEmpleado;
  TFicheroHistoricoCargos = file of THistoricoCargo;

  TVectorEmpleado = array [1..100] of TEmpleado;
  TVectorRegion = array [1..4] of TRegion;
  TVectorDepartamento = array [1..100] of TDepartamento;
  tVectorPais = array [1..100] of TPais;
  TVectorCargo = array [1..100] of TCargo;
  TVectorHistoricoCargos = array [1..200] of THistoricoCargo;
  TVectorConsultaEmplado = array [1..100] of TConsultaEmpleado;

  var
    p : TRegion;
    f : TFicheroRegiones;
    pp : TPais;
    fp : TFicheroPaises;
    pc : TCargo;
    fc : TFicheroCargos;
    pd : TDepartamento;
    fd : TFicheroDepartamentos;
    pe : TEmpleado;
    fe : TFicheroEmpleado;
    phc : THistoricoCargo;
    fhc : TFicheroHistoricoCargos;

    nRegion : Integer;
    vRegion : TVectorRegion;
    nEmpleado : integer;
    vEmpleado : TVectorEmpleado;
    nDepartamento : integer;
    vDepartamento : TVectorDepartamento;
    nPais : Integer;
    vPais : tVectorPais;
    nCargo : Integer;
    vCargo : TVectorCargo;
    nHistoricoCargo : Integer;
    vHistoricoCargo : TVectorHistoricoCargos;
    vConsultaEmpleado : TVectorConsultaEmplado;
    nConsultaEmpleado : Integer;


implementation
initialization
phc.id:= 1;
end.

