unit u_DAOCargos;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,u_Estructuras,Dialogs;

procedure altaCargo(var fc:TFicheroCargos; cargo:TCargo);
procedure crearFicheroC(var fc:TFicheroCargos);
procedure cargarFicheroCargo(var fc:TFicheroCargos);
procedure cargarVectorC(var v:TVectorCargo;var n:integer);
function buscarCargo(var fc:TFicheroCargos;codigo:longInt):longInt;
function buscarAbreviatura(var fc:TFicheroCargos;abBuscada: String):Boolean;
function proxID(var fc:TFicheroCargos):Integer;
procedure ModificarCargo(var fc: TFicheroCargos; pc: TCargo; posicion: longInt);


implementation

procedure crearFicheroC(var fc:TFicheroCargos);
begin
  assignFile(fc,FICHERO_CARGOS);
  try
    reset(fc);
  except
    rewrite(fc);
    closefile(fc);
  end;
end;

procedure cargarFicheroCargo(var fc:TFicheroCargos);
var
  pc :TCargo;
begin
  crearFicheroC(fc);
  reset(fc);

  pc.id := 1;
  pc.abreviatura:='AD_PRES';
  pc.tituloCargo:='Presidente';
  pc.salarioMin:=20080;
  pc.salarioMax:=40000;
  pc.estado:='Habilitado';
  write(fc,pc);
  pc.id := 2;
  pc.abreviatura:='AD_VP';
  pc.tituloCargo:='Vicepresidente';
  pc.salarioMin:=15000;
  pc.salarioMax:=30000;
  pc.estado:='Habilitado';
  write(fc,pc);
  pc.id := 3;
  pc.abreviatura:='AD_ASST';
  pc.tituloCargo:='Asistente de Administracion';
  pc.salarioMin:=3000;
  pc.salarioMax:=6000;
  pc.estado:='Habilitado';
  write(fc,pc);
  pc.id := 4;
  pc.abreviatura:='IT_PROG';
  pc.tituloCargo:='Programador';
  pc.salarioMin:=4000;
  pc.salarioMax:=10000;
  pc.estado:='Habilitado';
  write(fc,pc);
  pc.id := 5;
  pc.abreviatura:='MK_MAN';
  pc.tituloCargo:='Administrador de Marketing';
  pc.salarioMin:=9000;
  pc.salarioMax:=15000;
  pc.estado:='Habilitado';
  write(fc,pc);
  closeFile(fc);
end;

procedure altaCargo(var fc:TFicheroCargos; cargo:TCargo);
begin
 try
   reset(fc);
   seek(fc,fileSize(fc));
   write(fc,cargo);
 finally
   closeFile(fc)
 end;
end;

function buscarCargo(var fc:TFicheroCargos;codigo:longInt):longInt;
var
  i,posicion : longInt;
  cargo : TCargo;
begin
 try
   reset(fc);
   posicion := NO_ENCONTRADO;
   i := 0;

   while not eof(fc) and (posicion = NO_ENCONTRADO) do
       begin
         read(fc, cargo);
         if codigo = cargo.id then
             posicion:= i
         else
            i := i + 1;
       end;
    closeFile(fc);
 finally
   buscarCargo:=posicion;
 end;
end;

function buscarAbreviatura(var fc:TFicheroCargos;abBuscada: String):Boolean;
var
  cargo : TCargo;
  band : Boolean;
begin
  try
   reset(fc);
   band := False;
   while not eof(fc) and (band = False) do
       begin
         read(fc, cargo);
         if abBuscada = cargo.abreviatura then
             begin
              band := true;
             end;
       end;
    closeFile(fc);
 finally
   buscarAbreviatura:=band;
 end;
end;

function proxID(var fc:TFicheroCargos):Integer;
var
  cargo : TCargo;
  i: Integer;
  band : Boolean;
begin
  try
   reset(fc);
   band := False;
   i :=1 ;

   while (band = False) do
       begin
       while not eof(fc) and (band = False) do
           begin
             read(fc, cargo);
             if i = cargo.id then
                 band := True;
           end;
           if (band = False)then
               band:=True
           else
              begin
                   i:= i+1;
                   band := False;
              end;
       end;
     closeFile(fc);
 finally
   proxID := i;
 end;
end;

procedure ModificarCargo(var fc: TFicheroCargos; pc: TCargo; posicion: longInt);
begin
 try
  reset(fc);
  seek(fc, posicion);
  write(fc, pc);
 finally
   CloseFile(fc);
 end;
end;

procedure cargarVectorC(var v:TVectorCargo;var n:integer);
var
  fila : integer;
  R : TCargo;
begin
  cargarFicheroCargo(fc);
   try
    reset(fc);
    fila := 0;
    N := 0;
    while not eof(fc) do
       begin
        read(fc,R);
          begin
            fila := fila + 1;
            v[fila].id:= R.id;
            v[fila].abreviatura := R.abreviatura;
            v[fila].tituloCargo:= R.tituloCargo;
            v[fila].salarioMin := R.salarioMin;
            v[fila].salarioMax := R.salarioMax;
            v[fila].estado:= R.estado;
          end;
       end;
    n := fila;
    closeFile(fc);
    except
      on E: EInOutError do
      ShowMessage('Error: '+E.ClassName+'/'+E.Message)
  end;
end;

end.

