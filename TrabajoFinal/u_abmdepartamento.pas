unit u_ABMDepartamento;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, u_Estructuras,
  u_DAODepartamentos;

type

  { TfrmABMDepartamento }

  TfrmABMDepartamento = class(TForm)
    btnABM: TButton;
    btnSalir: TButton;
    cbxEstado: TCheckBox;
    cmbPais: TComboBox;
    edtCodigoPostal: TEdit;
    edtDireccion: TEdit;
    edtID: TEdit;
    edtNombre: TEdit;
    lblCodigoPostal: TLabel;
    lblDireccion: TLabel;
    lblEstado: TLabel;
    lblID: TLabel;
    lblNombre: TLabel;
    lblPais: TLabel;
    lblTitulo: TLabel;
    procedure btnABMClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private

  public
    procedure LimpiarCampos();
    procedure LeerDatos(var pd: TDepartamento);
  end;

var
  frmABMDepartamento: TfrmABMDepartamento;

implementation

{$R *.lfm}

procedure TfrmABMDepartamento.LimpiarCampos();
begin
  edtNombre.Text := '';
  edtDireccion.Text := '';
  edtCodigoPostal.Text := '';
  cmbPais.Text := '';
  cbxEstado.Checked := true;
end;


procedure TfrmABMDepartamento.LeerDatos(var pd: TDepartamento);
begin
   pd.id := StrToInt(edtID.Text);
   pd.nombre := edtNombre.Text;
   pd.direccion := edtDireccion.Text;
   pd.codigoPostal := edtCodigoPostal.Text;
   pd.pais.id := StrToInt(cmbPais.Text[1]);
   if cbxEstado.Checked = true then
     pd.estado := 'Habilitado'
   else
     if cbxEstado.Checked = false then
       pd.estado := 'Inhabilitado';
end;

procedure TfrmABMDepartamento.btnABMClick(Sender: TObject);
var
  posicion: longInt;
  val: boolean;
begin
    LeerDatos(pd);
    posicion := buscarID(fd, StrToInt(edtID.Text));
    val := validacionNombre(fd, edtNombre.Text);
    if (btnABM.Caption = 'Alta') then
      if (val = false) then
        begin
        NuevoDepartamento(fd, pd);
        LimpiarCampos();
        close;
        end
      else
        ShowMessage('El nombre ya existe')
     else
       if ((btnABM.Caption = 'Modificar') and (posicion <> NO_ENCONTRADO)) then
         begin
           ModificarDepartamento(fd, pd, posicion);
           LimpiarCampos();
           close;
         end
       else
         ShowMessage('El ID no se encuentra');
end;

procedure TfrmABMDepartamento.btnSalirClick(Sender: TObject);
begin
  close;
end;

end.

